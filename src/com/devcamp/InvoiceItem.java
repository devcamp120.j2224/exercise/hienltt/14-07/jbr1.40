package com.devcamp;

public class InvoiceItem {
    String id;
    String desc; //tên sản phẩm
    int qty;
    double unitPrice; //đơn gía

    public InvoiceItem(String id, String desc, int qty, double unitPrice){
        this.id = id;
        this.desc = desc;
        this.qty = qty;
        this.unitPrice = unitPrice;
    }
    public String getId() {
        return id;
    }
    public String setDesc() {
        return desc;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }
    public double getUnitPrice() {
        return unitPrice;
    }
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
    public double getTotal(){
        return this.unitPrice * this.qty;
    }
    @Override
    public String toString(){
        return "InvoiceItem[id= " + this.id + ", decs= " + this.desc + ", qty= " + this.qty + ", unitPrice= " + this.unitPrice + "]";
    }
}
