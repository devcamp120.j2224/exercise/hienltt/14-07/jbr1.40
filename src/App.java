import com.devcamp.InvoiceItem;

public class App {
    public static void main(String[] args) throws Exception {
        
        InvoiceItem invoiceItem1 = new InvoiceItem("STT01", "Iphone5", 2, 5500000);
        InvoiceItem invoiceItem2 = new InvoiceItem("STT03", "Airpod", 5, 3500000);

        System.out.println(invoiceItem1.toString());
        System.out.println(invoiceItem2.toString());

        System.out.printf("Total price of invoiceItem1:  %.0f VND\n", invoiceItem1.getTotal());
        System.out.printf("Total price of invoiceItem2: %.0f VND", invoiceItem2.getTotal()); 
    }
}